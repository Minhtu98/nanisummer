<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'test' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         ',t-H;l38BG97luCjLq0}khd8{pDY9-7z}()E=Q_K_MG N=?*1CD7z3x3u1_*+L;6' );
define( 'SECURE_AUTH_KEY',  'AiBm&0v-oR%6k6aQxI% w(t>8]JEPtkRKHc+s*|j|>6YN6Tw3fO0LeWl.~!1)X] ' );
define( 'LOGGED_IN_KEY',    ':f?AN-[gEz)w8Dl!q?E)zbKmV1_9S!4^AGz*kf-9(,,fg6C[^ia{b}y.! v_v=Qy' );
define( 'NONCE_KEY',        'hW0TJJmND*n~^0S*r!52M+qPJa$,yWHK[sLPUCfg1c4CZXS(!uc!oKyjsXyn%Er;' );
define( 'AUTH_SALT',        'B/H@Dt:b-}?7Jej2W2ufCy]G|#8qEhT;o IZ[TyyjlAD@|]UmO$L!7ngT<,HunL+' );
define( 'SECURE_AUTH_SALT', ',6Q0AgO<!tt`U&u;68>dN<Jjcnf?QS$HiehqNjA.|PQtye|;)gZ^}.c_]J< (goM' );
define( 'LOGGED_IN_SALT',   '&WdD#>GU)Kf=DJUBa|36>:}>w0.FUMfjg0aFdHP`4Nj<Z<o)%=!M2;%F#<eF!cHQ' );
define( 'NONCE_SALT',       ' c&Mel4;i4j~|1c63:4#XEb`6LK-N+0XlXZTcCFMWuNP8L9oH||_oV8ols;7K?qG' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
